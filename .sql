-- Create the database
CREATE DATABASE books;

-- Create a schema within the database
CREATE SCHEMA google_books;

-- Now, create the tables within the `google_books` schema.

-- books Table
CREATE TABLE google_books.books (
    ISBN (VARCHAR(20)),
    Title (TEXT),
    Author (VARCHAR(255)), 
    Rating (FLOAT), 
    Voters (INT),
    Price (DECIMAL(10, 2)),
    Currency (VARCHAR(3)), 
    Description (TEXT),
    Publisher (VARCHAR(255)),
    Page_Count (INT),
    Published_Date (DATE)

);

-- Genres Table
CREATE TABLE google_books.genres (
    Genre_ID (INT), 
    Genre_Name (VARCHAR(255)),
    Genre_Description (TEXT), 
    Genre_Image (BYTEA)
);

-- Languages Table
CREATE TABLE google_books.Languages (
     Language_ID (INT), 
     Language_Name (VARCHAR(50)),
     Language_Region (VARCHAR(50))
);

-- Authors Table
CREATE TABLE google_books.Authors (
    Author_ID (INT),
    Author_Name (VARCHAR(255)),
    Author_Biography (TEXT),
    Author_Nationality (VARCHAR(50))
);

--Publishers Table
CREATE TABLE google_books.Publishers (
     Publisher_ID (INT),
     Publisher_Name (VARCHAR(255)),
     Publisher_Location (VARCHAR(255)),
     Publisher_Contact (VARCHAR(20)));

);

-- Rating_Categories Table
CREATE TABLE google_books.Rating_Categories
   Rating_ID (INT),
   Rating_Category (VARCHAR(255)),
   Rating_Description (TEXT)
);

-- Book_Genres Table
CREATE TABLE google_books.Book_Genres
   BookGenre_ID (INT),
   ISBN (VARCHAR(20)),
   Genre_ID (INT)
);

-- Book_Languages Table
CREATE TABLE google_books.Book_Languages
   BookLanguage_ID (INT), 
   ISBN (VARCHAR(20)),
   Language_ID (INT)
);

-- Book_Authors Table
CREATE TABLE google_books.Book_Authors
    BookAuthor_ID (INT),
    ISBN (VARCHAR(20)),
    Author_ID (INT)
);

-- Book_Publishers Table

CREATE TABLE google_books.Book_publishers
    BookPublisher_ID (INT),
    ISBN (VARCHAR(20)),
    Publisher_ID (INT)
);

-- Book_Ratings Table
CREATE TABLE google_books.Book_Ratings
   BookRating_ID (INT),
   ISBN (VARCHAR(20)), 
   Rating_ID (INT)
);

